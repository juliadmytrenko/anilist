/**
 * @flow
 * @relayHash 9ed0290d5b44f3531e145b1f29b9b7e9
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AppFilterQueryVariables = {||};
export type AppFilterQueryResponse = {|
  +__typename: string,
  +filter: ?string,
|};
export type AppFilterQuery = {|
  variables: AppFilterQueryVariables,
  response: AppFilterQueryResponse,
|};
*/


/*
query AppFilterQuery {
  __typename
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "__typename",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ClientExtension",
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "filter",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "AppFilterQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "AppFilterQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "AppFilterQuery",
    "id": null,
    "text": "query AppFilterQuery {\n  __typename\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'c21fe8db26d05a1bac858fb0b7382afa';
module.exports = node;
