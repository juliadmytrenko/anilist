import React, { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { RelayEnvironmentProvider, preloadQuery, usePreloadedQuery } from 'react-relay/hooks';
import { commitLocalUpdate } from 'react-relay';
import RelayEnvironment from './RelayEnvironment';
import styled from 'styled-components';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
const { Suspense } = React;

import Characters from './components/Characters';
import CharacterPage from './components/CharacterPage';
import Header from './components/Header';
import Footer from './components/Footer';

// notes
// load all on site but use lazy loading
// library dominant color for loading images
// make header and footer bg wider
// whn sb on characterPage and typing in search bar show suggested 10 if he clicks on one of them go directly to page if nor show characters component
// fan wiki

// CSS START

const Body = styled.div`
  background-color: floralwhite;
`;

const Wrapper = styled.div`
  a {
    text-decoration: none;
    color: inherit;
  }
`;

const Main = styled.main`
  /* width: 60vw; */
  max-width: 1400px;
  height: 100%;
  margin: 0 auto;
  padding: 10px;
`;

//  CSS END

// Define a query

commitLocalUpdate(RelayEnvironment, store => {
  store.getRoot().setValue(null, 'filter');
});

commitLocalUpdate(RelayEnvironment, store => {
  store.getRoot().setValue(1, 'currentPage');
});

commitLocalUpdate(RelayEnvironment, store => {
  store.getRoot().setValue(null, 'currentLastPage');
});

const FilterQuery = graphql`
  query AppFilterQuery {
    __typename
    filter
  }
`;

// Immediately load the query as our app starts. For a real app, we'd move this
// into our routing configuration, preloading data as we transition to new routes.

const preloadedQuery = preloadQuery(RelayEnvironment, FilterQuery, {
  /* query variables */
});

// Inner component that reads the preloaded query results via `usePreloadedQuery()`.
// This works as follows:
// - If the query has completed, it returns the results of the query.
// - If the query is still pending, it "suspends" (indicates to React is isn't
//   ready to render yet). This will show the nearest <Suspense> fallback.
// - If the query failed, it throws the failure error. For simplicity we aren't
//   handling the failure case here.
const App = props => {
  const data = usePreloadedQuery(FilterQuery, props.preloadedQuery);

  useEffect(() => {});

  return (
    <Router>
      <Body className="App">
        <Wrapper>
          <Header />
          <Main>
            <Switch>
              <Route path="/characters">
                <Characters />
              </Route>
              <Route path="/character">
                <CharacterPage />
              </Route>
              <Route path={'/character/:id'} children={<CharacterPage />} />
              <Route path="/">
                <Characters />
              </Route>
            </Switch>
          </Main>
          <Footer />
        </Wrapper>
      </Body>
    </Router>
  );
};

// The above component needs to know how to access the Relay environment, and we
// need to specify a fallback in case it suspends:
// - <RelayEnvironmentProvider> tells child components how to talk to the current
//   Relay Environment instance
// - <Suspense> specifies a fallback in case a child suspends.
function AppRoot(props) {
  return (
    <RelayEnvironmentProvider environment={RelayEnvironment}>
      <Suspense fallback={'Loading...'}>
        <App preloadedQuery={preloadedQuery} />
      </Suspense>
    </RelayEnvironmentProvider>
  );
}

export default AppRoot;
