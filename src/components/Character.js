// Components CharacterTile, CharacterPage(po kliknieciu CharacterTile) w środdku kompoenntu Characters
// should include
// character info, image, after click go to the character s page
import React, { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

// CSS START
// add darker bg after hover
// load images in CharacterPage faster like on anilistpage
const Wrapper = styled.div`
  width: 100%;
  max-width: 260px;
  /* change height for smaller character cards */
  height: 100%;
  background-color: #fcfcfc;
  border-radius: 10px;
  text-align: center;
  border: 10px solid black;
  border-left: 5px solid black;
  border-right: 5px solid black;
  box-shadow: -8px 8px 3px rgba(0, 0, 0, 0.4);

  :hover {
    cursor: pointer;
    transform: scale(1.02);
    box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.4);
  }
`;

const Image = styled.img`
  border: 4px solid black;
  border-radius: 5px;
  max-width: 160px;
  margin: 15px;
  background-color: black;
`;

const Name = styled.span`
  display: block;
  font-family: 'Comic Sans MS', cursive, sans-serif;
  font-size: 1.5rem;
  font-weight: 600;
  padding: 0 5px 15px;
`;

//  CSS END

const Character = props => {
  const character = useFragment(
    graphql`
      fragment CharacterFragment on Character {
        whateverId: id
        name {
          first
          last
          full
        }
        image {
          large
        }
      }
    `,
    props.character
  );

  return (
    <>
      <Link
        to={{
          pathname: '/character/' + character.whateverId + '/' + character.name.first + '-' + character.name.last,
          state: { CharacterId: character.whateverId },
        }}>
        <Wrapper className="Character">
          <Image src={character.image.large}></Image>
          <Name>{character.name.full}</Name>
        </Wrapper>
      </Link>
    </>
  );
};

export default Character;
