/**
 * @flow
 * @relayHash ca25b479ad293ebcaad5de73cf725200
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type CharacterFragment$ref = any;
export type CharactersInfoQueryVariables = {|
  page?: ?number,
  perPage?: ?number,
  search?: ?string,
|};
export type CharactersInfoQueryResponse = {|
  +Page: ?{|
    +pageInfo: ?{|
      +total: ?number,
      +currentPage: ?number,
      +lastPage: ?number,
      +hasNextPage: ?boolean,
      +perPage: ?number,
    |},
    +characters: ?$ReadOnlyArray<?{|
      +whateverId: number,
      +name: ?{|
        +first: ?string,
        +last: ?string,
      |},
      +$fragmentRefs: CharacterFragment$ref,
    |}>,
  |}
|};
export type CharactersInfoQuery = {|
  variables: CharactersInfoQueryVariables,
  response: CharactersInfoQueryResponse,
|};
*/


/*
query CharactersInfoQuery(
  $page: Int
  $perPage: Int
  $search: String
) {
  Page(page: $page, perPage: $perPage) {
    pageInfo {
      total
      currentPage
      lastPage
      hasNextPage
      perPage
    }
    characters(search: $search) {
      whateverId: id
      ...CharacterFragment
      name {
        first
        last
      }
    }
  }
}

fragment CharacterFragment on Character {
  whateverId: id
  name {
    first
    last
    full
  }
  image {
    large
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "page",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "perPage",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "search",
    "type": "String",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "page",
    "variableName": "page"
  },
  {
    "kind": "Variable",
    "name": "perPage",
    "variableName": "perPage"
  }
],
v2 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "total",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "currentPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lastPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasNextPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "perPage",
      "args": null,
      "storageKey": null
    }
  ]
},
v3 = [
  {
    "kind": "Variable",
    "name": "search",
    "variableName": "search"
  }
],
v4 = {
  "kind": "ScalarField",
  "alias": "whateverId",
  "name": "id",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "first",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "last",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CharactersInfoQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "Page",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "Page",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "characters",
            "storageKey": null,
            "args": (v3/*: any*/),
            "concreteType": "Character",
            "plural": true,
            "selections": [
              (v4/*: any*/),
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "name",
                "storageKey": null,
                "args": null,
                "concreteType": "CharacterName",
                "plural": false,
                "selections": [
                  (v5/*: any*/),
                  (v6/*: any*/)
                ]
              },
              {
                "kind": "FragmentSpread",
                "name": "CharacterFragment",
                "args": null
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CharactersInfoQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "Page",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "Page",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "characters",
            "storageKey": null,
            "args": (v3/*: any*/),
            "concreteType": "Character",
            "plural": true,
            "selections": [
              (v4/*: any*/),
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "name",
                "storageKey": null,
                "args": null,
                "concreteType": "CharacterName",
                "plural": false,
                "selections": [
                  (v5/*: any*/),
                  (v6/*: any*/),
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "full",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "image",
                "storageKey": null,
                "args": null,
                "concreteType": "CharacterImage",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "large",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "CharactersInfoQuery",
    "id": null,
    "text": "query CharactersInfoQuery(\n  $page: Int\n  $perPage: Int\n  $search: String\n) {\n  Page(page: $page, perPage: $perPage) {\n    pageInfo {\n      total\n      currentPage\n      lastPage\n      hasNextPage\n      perPage\n    }\n    characters(search: $search) {\n      whateverId: id\n      ...CharacterFragment\n      name {\n        first\n        last\n      }\n    }\n  }\n}\n\nfragment CharacterFragment on Character {\n  whateverId: id\n  name {\n    first\n    last\n    full\n  }\n  image {\n    large\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '9f3359de992759ccbbd2423f1f9c75da';
module.exports = node;
