/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type CharacterFragment$ref: FragmentReference;
declare export opaque type CharacterFragment$fragmentType: CharacterFragment$ref;
export type CharacterFragment = {|
  +whateverId: number,
  +name: ?{|
    +first: ?string,
    +last: ?string,
    +full: ?string,
  |},
  +image: ?{|
    +large: ?string
  |},
  +$refType: CharacterFragment$ref,
|};
export type CharacterFragment$data = CharacterFragment;
export type CharacterFragment$key = {
  +$data?: CharacterFragment$data,
  +$fragmentRefs: CharacterFragment$ref,
};
*/


const node/*: ReaderFragment*/ = {
  "kind": "Fragment",
  "name": "CharacterFragment",
  "type": "Character",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": "whateverId",
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "name",
      "storageKey": null,
      "args": null,
      "concreteType": "CharacterName",
      "plural": false,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "first",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "last",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "full",
          "args": null,
          "storageKey": null
        }
      ]
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "image",
      "storageKey": null,
      "args": null,
      "concreteType": "CharacterImage",
      "plural": false,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "large",
          "args": null,
          "storageKey": null
        }
      ]
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = 'b826de9d8c4b951d7664c0c7c8f83cd4';
module.exports = node;
