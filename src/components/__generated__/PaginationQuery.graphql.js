/**
 * @flow
 * @relayHash 337c1f270d562ddc9929a00fe917869c
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type PaginationQueryVariables = {||};
export type PaginationQueryResponse = {|
  +__typename: string,
  +currentPage: ?number,
  +currentLastPage: ?number,
|};
export type PaginationQuery = {|
  variables: PaginationQueryVariables,
  response: PaginationQueryResponse,
|};
*/


/*
query PaginationQuery {
  __typename
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "__typename",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ClientExtension",
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "currentPage",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "currentLastPage",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "PaginationQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "PaginationQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "PaginationQuery",
    "id": null,
    "text": "query PaginationQuery {\n  __typename\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '6fd5fceb8f3cd3dbf82947eafc364562';
module.exports = node;
