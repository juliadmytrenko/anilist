/**
 * @flow
 * @relayHash 217d4638d626dbd028c49456ece59ac3
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CharacterPageQueryVariables = {|
  id?: ?number
|};
export type CharacterPageQueryResponse = {|
  +Character: ?{|
    +name: ?{|
      +first: ?string,
      +last: ?string,
      +full: ?string,
      +native: ?string,
      +alternative: ?$ReadOnlyArray<?string>,
    |},
    +image: ?{|
      +large: ?string
    |},
    +description: ?string,
    +media: ?{|
      +nodes: ?$ReadOnlyArray<?{|
        +title: ?{|
          +romaji: ?string,
          +english: ?string,
          +native: ?string,
          +userPreferred: ?string,
        |},
        +coverImage: ?{|
          +large: ?string,
          +medium: ?string,
          +color: ?string,
        |},
        +bannerImage: ?string,
      |}>
    |},
  |}
|};
export type CharacterPageQuery = {|
  variables: CharacterPageQueryVariables,
  response: CharacterPageQueryResponse,
|};
*/


/*
query CharacterPageQuery(
  $id: Int
) {
  Character(id: $id) {
    name {
      first
      last
      full
      native
      alternative
    }
    image {
      large
    }
    description(asHtml: false)
    media {
      nodes {
        title {
          romaji
          english
          native
          userPreferred
        }
        coverImage {
          large
          medium
          color
        }
        bannerImage
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "Int",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "native",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "large",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "Character",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Character",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "name",
        "storageKey": null,
        "args": null,
        "concreteType": "CharacterName",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "first",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "last",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "full",
            "args": null,
            "storageKey": null
          },
          (v1/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "alternative",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "image",
        "storageKey": null,
        "args": null,
        "concreteType": "CharacterImage",
        "plural": false,
        "selections": [
          (v2/*: any*/)
        ]
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "description",
        "args": [
          {
            "kind": "Literal",
            "name": "asHtml",
            "value": false
          }
        ],
        "storageKey": "description(asHtml:false)"
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "media",
        "storageKey": null,
        "args": null,
        "concreteType": "MediaConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "nodes",
            "storageKey": null,
            "args": null,
            "concreteType": "Media",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "title",
                "storageKey": null,
                "args": null,
                "concreteType": "MediaTitle",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "romaji",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "english",
                    "args": null,
                    "storageKey": null
                  },
                  (v1/*: any*/),
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "userPreferred",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "coverImage",
                "storageKey": null,
                "args": null,
                "concreteType": "MediaCoverImage",
                "plural": false,
                "selections": [
                  (v2/*: any*/),
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "medium",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "color",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "bannerImage",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CharacterPageQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v3/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "CharacterPageQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v3/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "CharacterPageQuery",
    "id": null,
    "text": "query CharacterPageQuery(\n  $id: Int\n) {\n  Character(id: $id) {\n    name {\n      first\n      last\n      full\n      native\n      alternative\n    }\n    image {\n      large\n    }\n    description(asHtml: false)\n    media {\n      nodes {\n        title {\n          romaji\n          english\n          native\n          userPreferred\n        }\n        coverImage {\n          large\n          medium\n          color\n        }\n        bannerImage\n      }\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '34127a0386ee36915122a5fa09207b60';
module.exports = node;
