// Components CharacterTile, CharacterPage(po kliknieciu CharacterTile) w środdku kompoenntu Characters
// should include
// character info, image, after click go to the character s page
import React, { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useQuery } from 'relay-hooks';
import { useLocation } from 'react-router';
import { useParams } from 'react-router-dom';

import styled from 'styled-components';
// CSS START
const Wrapper = styled.div`
  width: 100%;
  background-color: #fffefc;
`;
const Grid = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  grid-template-rows: repeat(auto-fill, minmax(100px, 500px));
  grid-row-gap: 30px;
  grid-column-gap: 15px;

  justify-items: center;
  align-items: center;
  > * {
  }
  .katakana {
    z-index: 2;
    display: block;
    font-size: 70px;
    font-weight: bolder;
    color: rgba(255, 182, 193, 0.8);
    transform: rotate(-5deg);
    position: absolute;
    top: -10px;
    left: 30px;
  }
`;

const Image = styled.img`
  grid-column-start: 1;
  border: 4px solid black;
  border: 10px solid black;
  border-left: 5px solid black;
  border-right: 5px solid black;
  border-radius: 10px;
  background-color: black;
`;

const Description = styled.div`
  max-height: 650px;
  position: relative;
  grid-column-start: 2;
  grid-column-end: -1;
  grid-row-start: 1;
  grid-row-end: ${({ ReadMore }) => (ReadMore ? '2' : '4')};

  overflow: hidden;
  /* grid-row-end: 4; */

  font-size: 20px;
  overflow-y: auto;
  align-self: start;
  margin: 30px;
  margin-top: 60px;

  p {
    margin: 0;
    padding: 5px;
    font-size: 22px;
  }
`;

const Name = styled.div`
  grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  font-size: 24px;
  border: 6px dashed black;
  padding: 30px;

  span {
    font-size: 1.5rem;
    font-weight: bold;
    font-family: 'Comic Sans MS', cursive, sans-serif;
  }
  ul {
    margin: 5px;
    padding-left: 5px;
    text-align: left;
    li {
      margin-left: 20px;
    }
  }
`;

const Media = styled.div`
  z-index: 2;
  width: 96%;
  grid-column-start: 1;
  grid-column-end: -1;
  margin-top: 20px;

  li {
    display: block;

    /* width: 100%; */
    border: 4px solid black;
    border: 10px solid black;
    border-left: 5px solid black;
    border-right: 5px solid black;
    border-radius: 10px;
    text-align: center;
    font-size: 20px;
    font-family: 'Comic Sans MS', cursive, sans-serif;
    padding: 0 8px 8px 8px;
    margin: 0 20px;
    background: white;
    box-shadow: -8px 8px 3px rgba(0, 0, 0, 0.4);

    :hover {
      cursor: pointer;
      transform: scale(1.02);
      box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.4);
    }
  }

  ul {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    grid-row-gap: 20px;
    grid-column-gap: 40px;
    /* justify-items: center; */
    margin: 0 20px;
    padding: 0;
    justify-items: center;
  }
  img {
    display: block;
    max-height: 300px;
    max-width: 200px;
    margin: 0 auto;
    padding: 16px;
  }

  h3 {
    margin: 0;
  }
`;

//  CSS END

// Define a query
const CharacterPageQuery = graphql`
  query CharacterPageQuery($id: Int) {
    Character(id: $id) {
      name {
        first
        last
        full
        native
        alternative
      }
      image {
        large
      }
      description(asHtml: false)
      media {
        nodes {
          title {
            romaji
            english
            native
            userPreferred
          }
          coverImage {
            large
            medium
            color
          }
          bannerImage
        }
      }
    }
  }
`;

const CharacterPage = () => {
  let { id } = useParams();
  const CharacterId = id ? id : useLocation().state.CharacterId;
  const { props, error } = useQuery(CharacterPageQuery, { id: CharacterId });
  let isReadMore = false;

  useEffect(() => {
    console.log('Params: ', id);
  });

  if (error) {
    return <div>{error.message}</div>;
  } else if (props) {
    console.log('dlugosc opisu: ', props.Character.description.length);
    if (props.Character.description.length > 2000) {
      isReadMore = true;
    }

    return (
      <Wrapper className="Character">
        <Grid>
          <span className="katakana">{props.Character.name.native}</span>
          <Image src={props.Character.image.large}></Image>
          <Name>
            <div>
              <span>Full: </span>
              {props.Character.name.full}
            </div>
            <div>
              <span>First: </span>
              {props.Character.name.first}
            </div>
            <div>
              <span>Last: </span>
              {props.Character.name.last}
            </div>

            {props.Character.name.alternative[0] ? (
              <div>
                <ul>
                  <span>Alternative: </span>
                  {props.Character.name.alternative.map((alternativeName, index) => (
                    <li key={index}>{alternativeName}</li>
                  ))}
                </ul>
              </div>
            ) : (
              ''
            )}
          </Name>
          <Description ReadMore={isReadMore}>
            {props.Character.description ? (
              <p dangerouslySetInnerHTML={{ __html: props.Character.description }}></p>
            ) : (
              <p>No description （´＿｀）</p>
            )}
          </Description>
          <Media>
            <ul>
              {props.Character.media.nodes.map((node, index) => {
                return (
                  <li key={index}>
                    <img src={node.coverImage.large} />
                    <h3>{node.title.english}</h3>
                  </li>
                );
              })}
            </ul>
          </Media>
        </Grid>
      </Wrapper>
    );
  }
  return <div></div>;
};

export default CharacterPage;
