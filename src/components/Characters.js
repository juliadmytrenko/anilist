import React, { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import RelayEnvironment from './../RelayEnvironment';

import styled from 'styled-components';
import Character from './Character';
import { useQuery } from 'relay-hooks';
import Loadable from 'react-loadable';
import { commitLocalUpdate } from 'react-relay';
import Pagination from './Pagination';

// CSS START
const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  grid-row-gap: 30px;
  grid-column-gap: 30px;
  /* align-items: start ; */

  justify-items: center;
  > * {
  }
`;

const LoadingScreen = styled.div`
  grid-column: 1 -1;
  text-align: center;
  font-size: 100px;
  font-weight: 900;
`;

const SortBy = styled.div`
  grid-column-start: 1;
  grid-column-end: -1;
  width: 100%;
  height: 100px;
  text-align: center;
  font-size: 100px;
  font-weight: 900;
  /* border: 1px solid black; */
  display: flex;
  margin-bottom: 10px;

  .dropdown {
    flex: 1;
  }
  .filters {
    flex: 5;
  }

  select {
    width: 300px;
    height: 50px;
    font-size: 30px;
  }
`;

// CSS END

const query = graphql`
  query CharactersInfoQuery($page: Int, $perPage: Int, $search: String) {
    Page(page: $page, perPage: $perPage) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      characters(search: $search) {
        whateverId: id
        ...CharacterFragment
        name {
          first
          last
        }
      }
    }
  }
`;

const Characters = () => {
  const [sortBy, setSortBy] = useState('default');
  // const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(24);

  // dodac first page and last page button
  // gdy duzo stron dodac trzykropek 1 2 3 4 5 6 7 8 9 ... 10023
  //    <:first page    <-- 1 2 3 -->       last page:>

  useEffect(() => {});

  const data = useQuery(
    graphql`
      query CharactersQuery {
        __typename
        filter
        currentPage
      }
    `,
    null,
    { fetchPolicy: 'store-only' }
  );

  const page = data.props.currentPage;

  const { error, props } = useQuery(
    query,
    {
      page: page,
      perPage: perPage,
      search: data.props.filter,
    },
    { fetchPolicy: 'store-or-network' }
  );

  console.log('currentPage', data.props.currentPage);

  if (error) {
    return <div>{error.message}</div>;
  } else if (props) {
    commitLocalUpdate(RelayEnvironment, store => {
      store.getRoot().setValue(props.Page.pageInfo.lastPage, 'currentLastPage');
    });

    console.log('last Page characters: ', props.Page.pageInfo.lastPage);

    let charactersSorted;
    if (sortBy == 'default') charactersSorted = props.Page.characters;
    else if (sortBy == 'alphabetically')
      charactersSorted = props.Page.characters
        .slice()
        .sort((a, b) =>
          a.name.last == b.name.last ? (a.name.first == b.name.first ? 1 : -1) : a.name.last > b.name.last ? 1 : -1
        );
    const characters = charactersSorted.map((character, index) => {
      return <Character key={index} character={character}></Character>;
    });

    return (
      <Grid className="scrollable">
        <SortBy>
          <div className="dropdown" onChange={e => setSortBy(e.target.value)}>
            <select defaultValue="default">
              <option value="default">default</option>
              <option value="alphabetically">alphabetically</option>
            </select>
          </div>
          <div className="filters">
            <div className="perPage" onChange={e => setPerPage(e.target.value)}>
              <select defaultValue={perPage}>
                <option value="24">24</option>
                <option value="48">48</option>
              </select>
            </div>
          </div>
        </SortBy>
        {characters != '' && <Pagination />}
        {characters}
        {characters != '' && <Pagination />}
      </Grid>
    );
  }
  return <LoadingScreen>(∪｡∪)｡｡｡zzZ</LoadingScreen>;
};

export default Characters;
