import React, { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useQuery } from 'relay-hooks';
import RelayEnvironment from './../RelayEnvironment';
import styled from 'styled-components';
import { commitLocalUpdate } from 'react-relay';

// CSS START
const Wrapper = styled.div`
  width: 100%;
  grid-column-start: 1;
  grid-column-end: -1;
  text-align: center;
  display: flex;

  .buttons {
    grid-column-start: 2;
    grid-column-end: -2;
    flex: 1;
  }
  button {
    background-color: black;
    color: lightgrey;
    min-width: 60px;
    height: 60px;
    border-radius: 50%;
    margin: 0 10px;
    font-weight: bold;
    font-size: 30px;
    font-family: 'Comic Sans MS', cursive, sans-serif;

    :focus {
      outline: 0;
    }
  }
`;
//  CSS END

const Pagination = () => {
  //zamiast numberof przekaac last page i na podstawie tego obliczyc ilosc buttonow i dodac lastPage do ifa na dole
  // zrobic tez tak ze gdy przechodzisz na nastepna strone to vartosci w buttonach sie zmieniaja
  // input wpisz strone

  const { props } = useQuery(
    graphql`
      query PaginationQuery {
        __typename
        currentPage
        currentLastPage
      }
    `,
    null,
    { fetchPolicy: 'store-only' }
  );

  const { currentLastPage } = props;

  const handleOnClick = event => {
    if (event.target.tagName == 'BUTTON') {
      commitLocalUpdate(RelayEnvironment, store => {
        const currentPage = store.getRoot().getValue('currentPage');
        const currentLastPage = store.getRoot().getValue('currentLastPage');
        let currentPageNewValue = currentPage;
        if (event.target.className.includes('nextPage') && currentPage < currentLastPage - 2)
          currentPageNewValue = currentPage + 1;
        else if (event.target.className.includes('nextPage')) currentPageNewValue = currentPage;
        // dodac warunek if(currentPage<lastPage)
        else if (event.target.className.includes('previousPage') && currentPage >= 2)
          currentPageNewValue = currentPage - 1;
        else if (event.target.className.includes('previousPage')) currentPageNewValue = currentPage;
        else if (event.target.className.includes('firstPage')) currentPageNewValue = 1;
        else if (event.target.className.includes('lastPage')) currentPageNewValue = currentLastPage;
        else currentPageNewValue = event.target.innerHTML;
        store.getRoot().setValue(currentPageNewValue, 'currentPage');
      });
    }
  };

  if (props) {
    let numberOfPaginationButtons = 0;
    currentLastPage > 20 ? (numberOfPaginationButtons = 5) : (numberOfPaginationButtons = currentLastPage);

    return (
      <Wrapper onClick={event => handleOnClick(event)}>
        <button className="firstPage">&#171;</button>
        <button className="previousPage">&#8249;</button>
        <div className="buttons">
          {Array(numberOfPaginationButtons)
            .fill()
            .map((_, i) => (
              <button key={i}>{i + 1}</button>
            ))}{' '}
          {currentLastPage > 5 && (
            <>
              {' '}
              &#x25cf; &#x25cf; &#x25cf; <button>{currentLastPage}</button>
            </>
          )}
        </div>
        <button className="nextPage">&#8250;</button>
        <button className="lastPage">&#187;</button>
      </Wrapper>
    );
  }
};

export default Pagination;
