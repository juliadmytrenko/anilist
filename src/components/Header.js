// Components CharacterTile, CharacterPage(po kliknieciu CharacterTile) w środdku kompoenntu Characters
// should include
// character info, image, after click go to the character s page
import React from 'react';
import RelayEnvironment from './../RelayEnvironment';
import { commitLocalUpdate } from 'react-relay';
import styled from 'styled-components';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import { createGlobalStyle } from 'styled-components';
import windows from './../assets/images/windows.png';

// CSS START

const GlobalStyles = createGlobalStyle`
  @import url("https://fonts.googleapis.com/css?family=Kulim+Park&display=swap");
  body {
    font-family: 'Kulim Park', sans-serif;
  }
`;

const Wrapper = styled.div`
  width: 100%;
  background-color: black;
  margin-bottom: 50px;
  color: white;
`;

const Navigation = styled.nav`
  max-width: 1400px;
  height: 100%;
  margin: 0 auto;
  padding: 10px;
  height: 200px;

  position: relative;
  .logo {
    flex: 2;
  }

  .logo-bg {
    display: block;
    max-width: 120px;
  }

  nav {
    width: 100%;
    ul {
      display: flex;
      list-style: none;
      text-align: center;
    }
    li {
      flex: 1;
      font-size: 30px;
    }

    a {
      text-decoration: none;
      color: white;
      font-weight: 900;
      /* font-family: "Arial Black" */
    }
  }
`;

const SearchBox = styled.div`
  /* font-family: Arial, Helvetica, sans-serif; */
  font-size: 40px;
  position: absolute;
  bottom: 0;
  margin: 15px;
  input {
    width: 300px;
    height: 50px;
    font-size: 30px;
    border-radius: 10px;
    padding: 5px;
    margin: 0 15px;
  }
`;
//  CSS END

const Header = () => {
  return (
    <Wrapper>
      <GlobalStyles />
      <Navigation>
        {/* <FanFavourites>Fan Favourites</FanFavourites> */}

        <nav>
          <ul>
            <li className="logo">
              <Link to="/">
                <img className="logo-bg" src={windows} />
              </Link>
            </li>
            <li>
              <Link to="/manga">MANGA</Link>
            </li>
            <li>
              <Link to="/anime">ANIME</Link>
            </li>
            <li>
              <Link to="/characters">CHARACTERS</Link>
            </li>
          </ul>
        </nav>

        <SearchBox>
          <label>
            search character:
            <input
              type="search"
              name="searchBox"
              onChange={event => {
                commitLocalUpdate(RelayEnvironment, store => {
                  store.getRoot().setValue(event.target.value == '' ? null : event.target.value, 'filter');
                  store.getRoot().setValue(0, 'currentPage');
                });
              }}></input>
          </label>
        </SearchBox>
      </Navigation>
    </Wrapper>
  );
};

export default Header;
