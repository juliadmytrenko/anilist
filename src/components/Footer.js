// Components CharacterTile, CharacterPage(po kliknieciu CharacterTile) w środdku kompoenntu Characters
// should include
// character info, image, after click go to the character s page
import React, { useEffect, useState } from 'react';
import fetchGraphQL from './../fetchGraphQL';
import graphql from 'babel-plugin-relay/macro';
import { QueryRenderer } from 'react-relay';
import { fetchQuery } from 'react-relay/hooks';
import RelayEnvironment from './../RelayEnvironment';
import { commitLocalUpdate } from 'react-relay';
import styled from 'styled-components';

// CSS START

const Wrapper = styled.div`
  width: 100%;
  height: 200px;
  background-color: black;
  margin-top: 60px;
  margin-bottom: 50px;
  color: white;
`;

//  CSS END

const Footer = () => {
  return <Wrapper></Wrapper>;
};

export default Footer;
